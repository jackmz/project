import { SourceFile, Node, ts, NewExpression, PropertyAccessExpression, ClassDeclaration } from "ts-morph";

type classDec = {[x: string]: ClassDeclaration};

export class FileReading {
    private startFile: SourceFile | undefined;
    private listOfNodeNames: string[] = [];

    private classDecs: classDec[] = [];

    printClassDecs() {
        console.log(this.classDecs);
    }

    constructor(startFile: SourceFile | undefined) {
        this.startFile = startFile;
    }

    printImports(file: SourceFile | undefined, count: number) {
        let line = "";
        for (let i = 0; i < count; i++) {
            line += "\t"
        }
        console.log(line + file?.getBaseName());
    
        let imports = file?.getImportDeclarations();
      
        if (typeof imports !== 'undefined' && imports.length !== 0) {
          //console.log("Imported from " + file?.getBaseName());
      
          for (let importFile of imports) {
            let nextFile = importFile.getModuleSpecifierSourceFile();
            
            this.printImports(nextFile, count + 1);
      
          }
        } else {
          //console.log(line + "\tNo imports");
        }
      
    }

    printFileClasses(file: SourceFile | undefined) {
        if (typeof file !== 'undefined') {
            console.log("\n" +file.getBaseName());
            const classes = file.getClasses();
            if (classes.length > 0)
                console.log("Classes:");
    
            for (let c of classes) {
                console.log(c.getName());
                const methods = c.getInstanceMethods();
    
                for (let m of methods) {
                console.log("\t" + m.getName());
                }
    
                //const properties = c.getProperties();
                
    
            }
    
            const functions = file.getFunctions();
            if (functions.length > 0)
                console.log("Functions:");
    
            for (let f of functions) {
                console.log(f.getName());
            }
    
        } else {
            console.log("File was undefined");
        }
    
    }

    printFilesClasses(files: SourceFile[]) {
        for (let file of files) {
            this.printFileClasses(file);
            
        }
    
    }

    navigateAST(node: Node, count: number) {
        if (count === -1) {
            this.listOfNodeNames = [];
            this.classDecs = [];
            count = 0;
        }
        let line = "";
        for (let i = 0; i < count; i++) {
            line += "\t"
        }

        // probably better to check for CallExpression
        if (node.getKind() === ts.SyntaxKind.Identifier) {
            if (!this.listOfNodeNames.includes(node.getText()) ) {
                this.listOfNodeNames.push(node.getText());
                
                console.log(line + node.getText());

                let imports = this.startFile?.getImportDeclarations();
                if (typeof imports !== 'undefined' && imports.length !== 0) {
                    for (let importFile of imports) {
                        // look for function
                        let srcFile = importFile.getModuleSpecifierSourceFile();
                        let funcDec = srcFile?.getFunction(node.getText());

                        if (typeof funcDec !== 'undefined') {
                            this.navigateAST(funcDec, count + 1);
                        }

                        // look for class instance declaration
                        let classD = srcFile?.getClass(node.getText());
                        if (typeof classD !== 'undefined') {
                            //console.log(classD.getName()); 
                            let x = node.getFirstAncestorByKind(ts.SyntaxKind.VariableDeclaration);
                            let y = x?.getFirstChildByKind(ts.SyntaxKind.Identifier);
                            // y should be the name of the class instance
                            if (typeof y !== 'undefined') {
                                let instName = y.getText();
                                this.classDecs.push({[instName]: classD});
                                console.log("Found class instance of name " + instName);
                                
                            }
                        }


                    
                        

                    }

                } 

            } else {
                // check for class method being called
                for (let c of this.classDecs) {
                    let name = Object.getOwnPropertyNames(c);
                    
                    if (node.getText() === name[0]) {
                        // find the method name associated with this node
                        let expression = node.getParent();
                        let methodName = expression?.getLastChildByKind(ts.SyntaxKind.Identifier);
                        if (typeof methodName !== 'undefined') {
                            // find the particular method that node is calling
                            let method = c[name[0]].getInstanceMethod(methodName.getText());
                            if (typeof method !== 'undefined') {
                                console.log(line + expression?.getText());
                                this.navigateAST(method, count + 1);
                            }
                        }
                    }

                }
            }
            
            return;
        }
         
        node.forEachChild(node => {
            this.navigateAST(node, count);
        });
    
    }

}