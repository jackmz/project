export function printPower(x: number, y: number) {
    let result = power(x, y);
    console.log(`${x} to the power of ${y} = ${result}`);
}

function power(x:number, y:number) {
    return Math.pow(x, y);
}