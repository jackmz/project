import { readFileSync } from "fs";
const data = readFileSync('./src/main.ts', "utf8");

// const esprima = require('esprima');
// let syntax = esprima.parseModule(data);
// console.log(syntax);

// const {parse} = require('abstract-syntax-tree');
// const x = parse(data);
// console.log(x);


import ts, { SyntaxKind } from "typescript";

const node = ts.createSourceFile(
  'x.ts',   // fileName
  data, // sourceText
  ts.ScriptTarget.Latest // languageVersion
);

function navigateAST(node: ts.Node) {
  console.log(node.getText());

  ts.forEachChild(node, navigateAST);
}

navigateAST(node);

// let classDec: any[] = [];
// console.log("\nClass Declarations:")
// node.forEachChild((child: any) => {
//   if (ts.SyntaxKind[child.kind] === 'ClassDeclaration')
//     classDec.push(child);
// });

// for (let i = 0; i < classDec.length; i++) {
// console.log(classDec[i].name.escapedText);
// }