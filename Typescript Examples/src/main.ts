import { printSomething } from './printing';
import * as p from './PrintingNumbers';

printSomething(4, 6);

const numPrint = new p.PrintingNumbers();
numPrint.setX(4);
numPrint.setY(6);

numPrint.calc.power(numPrint.getX(), numPrint.getY());
numPrint.printResult();