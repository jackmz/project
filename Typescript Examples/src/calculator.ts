export class Calculator {
    static result = 0;

    power(x:number, y:number) {
        Calculator.result = Math.pow(x, y);
    }

    multiply(x: number, y:number) {
        Calculator.result = x * y;
    }

    dot(listx: number[], listy:number[]) {
        let result = 0;
        if (listx.length === listy.length)
            for (let i = 0; i < listx.length; i++) {
                result += listx[i] * listy[i];
            }
        else 
            console.log("Lists aren't same length");
        Calculator.result = result;
    }

    maximum(numbers: number[]) {
        let max = numbers[0];
        for (let i = 1; i < numbers.length; i++) {
            if (numbers[i] > max)
                max = numbers[i];
        }
        //return max;
        Calculator.result = max;
    }

    getResult() {
        return Calculator.result;
    }

}