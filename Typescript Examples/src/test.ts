import { Project, ts } from "ts-morph";
import { FileReading } from './FileReading'

const project = new Project({
  tsConfigFilePath: "./tsconfig.json",
});

const srcFile = project.getSourceFile("./src/PrintingNumbers.ts");
const srcFiles = project.getSourceFiles();
const startFile = project.getSourceFile("./src/main.ts");

const reading = new FileReading(startFile);

//reading.printImports(startFile, 0);
//reading.printFileClasses(srcFile);
// reading.printFilesClasses(srcFiles);

if (typeof startFile !== 'undefined') {
  reading.navigateAST(startFile, -1);
}
