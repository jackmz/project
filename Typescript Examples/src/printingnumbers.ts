import {Calculator} from "./Calculator"

export class PrintingNumbers {
    private x: number = 0;
    private y: number = 0;

    calc: Calculator = new Calculator();

    constructor(x?: number, y?: number) {
        if (typeof x === 'number') {
        this.x = x;
        }
        if(typeof y === 'number') {
        this.y = y;
        }
    }
    setX(num: number) {
        this.x = num;
    }
    setY(num: number) {
        this.y = num;
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }

    printResult() {
        console.log(this.calc.getResult());
    }


}