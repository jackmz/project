import {printPower} from './power';

export function printSomething(x: number, y:number) {
    printPower(x, y);
    printPower(y, x);
}